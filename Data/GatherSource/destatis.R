#############
# Download and clean Auftragseingang (Wert- und Volumenindex) im Bauhauptgewerbe, 
# Ergebnis - 44111-0008
# Sebastian Sukstorf
# Updated 4 Januar 2014 
# Data downloaded from https://www-genesis.destatis.de
#############

# Load library
library(repmis)
library(plyr)

# Workdirectory setzen
setwd("E:/bit_repositories/produktionbaugewerbe/Data/GatherSource")

# Lesen der unbehandelten Roh-Daten aus 44111-0008.csv Datei 
AuftBau <- read.csv(url("https://www-genesis.destatis.de/genesis/online/link/tabelleDownload/44111-0008.csv"),
                    sep=";",
                    dec=",",
                    header = FALSE)

# Subset der zu verwenden Daten (Zeile 11-33)
AuftBau <- AuftBau[12:33,1:2]

# Variablen umbenennen
AuftBau <- rename(x = AuftBau,
                      replace = c("V1" = "Jahr",
                                  "V2" = "HochTiefbauWert"
                                  ))

# Speichere bearbeitet Daten als MainData.csv
write.table(AuftBau, "/bit_repositories/produktionbaugewerbe/Data/GatherSource/MainData.csv",
            sep=";")

