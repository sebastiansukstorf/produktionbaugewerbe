# Produktion im Baugewerbe  
## Sebastian Sukstorf  
### Update: 04.01.2014  

Erstellt von: Sebastian Sukstorf  
Unternehmen: D+H Mechatronic AG  
Mail: sebastian.sukstorf@dh-partner.com  

Analyse von relevanten Statistiken für das Baugewerbe / Construction

----

Folgende Daten werden verwendet

1. [Eurostat: *Produktion im Baugewerbe - monatliche Daten, Wachstumsraten (Tabelle: sts_coptgr_m)*](http://appsso.eurostat.ec.europa.eu/nui/show.do?dataset=sts_coprgr_m&lang=de)
2. [Destatis: *Auftragseingang (Wert- und Volumenindex) im Bauhauptgewerbe (Betriebe mit 20 u.m. tätigen Personen): Deutschland, Jahre, Bauarten (Tabelle: 44111-0008)*](https://www-genesis.destatis.de/genesis/online/link/tabelleDownload/44111-0008.csv)

## Session Info
The current version of the manuscript was compiled with [RStudio](http://www.rstudio.com/) (Version 0.98.490 – © 2009-2013 RStudio, Inc.) with the following R session:


```
## R version 3.0.2 (2013-09-25)
## Platform: i386-w64-mingw32/i386 (32-bit)

## locale:
## [1] LC_COLLATE=German_Germany.1252  LC_CTYPE=German_Germany.1252    LC_MONETARY=German_Germany.1252
## [4] LC_NUMERIC=C                    LC_TIME=German_Germany.1252    

## attached base packages:
## [1] stats     graphics  grDevices utils     datasets  methods   base     

## loaded via a namespace (and not attached):
## [1] tools_3.0.2
```

---

&copy; Sebastian Sukstorf (2013)
